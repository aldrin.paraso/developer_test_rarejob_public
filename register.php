<?php include('session.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Register</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
  <div class="header">
  	<h2>Register</h2>
  </div>

  <form method="post" action="register.php">
  	<?php include('errors.php'); ?>
  	<div class="form-group col-md-6">
  	  <label>First Name</label>
  	  <input class="form-control" type="text" name="firstname" value="<?php echo $firstname; ?>">
  	</div>
    <div class="form-group col-md-6">
  	  <label>Last Name</label>
  	  <input class="form-control" type="text" name="lastname" value="<?php echo $lastname; ?>">
  	</div>
    <div class="form-group col-md-6">
  	  <label>Username</label>
  	  <input class="form-control" type="text" name="username" value="<?php echo $username; ?>">
  	</div>
  	<div class="form-group col-md-6">
  	  <label>Email</label>
  	  <input class="form-control" type="email" name="email" value="<?php echo $email; ?>">
  	</div>
  	<div class="form-group col-md-6">
  	  <label>Password</label>
  	  <input class="form-control" type="password" name="password_1">
  	</div>
  	<div class="form-group col-md-6">
  	  <label>Confirm password</label>
  	  <input class="form-control" type="password" name="password_2">
  	</div>
  	<div class="form-group col-md-6">
  	  <button class="btn btn-primary" type="submit" name="reg_user">Register</button>
  	</div>
  	<p>
  		Already a member? <a href="login.php">Sign in</a>
  	</p>
  </form>
</body>
</html>