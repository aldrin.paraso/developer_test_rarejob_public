
function fibonacci($num){
  if ($num < 0){
    return "No negative fibonacci numbers.";
  }
  
  if ($num <= 1){
    return $num;
  }
  
  $prev_fib = 0;
  $curr_fib = 1;
  for ($i = 0; $i < $num - 1; $i++) {
    $new_fib = $prev_fib + $curr_fib;
    $prev_fib = $curr_fib;
    $curr_fib = $new_fib;
  }
  
  return $curr_fib;

}

echo fibonacci(8); /* 21 */
echo fibonacci(0); /* 0 */
echo fibonacci(-1); /* No negative fibonacci numbers. */

